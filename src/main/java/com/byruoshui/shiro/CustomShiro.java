package com.byruoshui.shiro;

import com.byruoshui.lazyadmin.entity.Account;
import com.byruoshui.lazyadmin.service.AccountService;
import com.byruoshui.lazyadmin.service.MenuService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.HashSet;
import java.util.Set;

public class CustomShiro extends AuthorizingRealm {

    @Autowired
    private AccountService accountService;
    @Autowired
   private MenuService menuService;
    /**
     * 验证用户角色和权限
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        System.out.println("开始验证权限");
        String primaryPrincipal = (String) principalCollection.getPrimaryPrincipal();
        Account account=accountService.findByUsername(primaryPrincipal);
        if (!ObjectUtils.isEmpty(account)) {
            SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
            System.out.printf("ID:%d,用户名:%s", account.getId(), account.getUsername());
            System.out.println();
            Set<String> roles = new HashSet<String>();
            Set<String> perms = new HashSet<String>();
            if (!CollectionUtils.isEmpty(account.getRoles())) {
                account.getRoles().forEach(role -> {
                    roles.add(role.getCode());
                    if (!CollectionUtils.isEmpty(role.getPerms()))
                    {
                        role.getPerms().forEach(perm -> {
                            perms.add(perm.getCode());
                        });
                    }
                });
            }
            if (!CollectionUtils.isEmpty(roles))
                simpleAuthorizationInfo.setRoles(roles);
            if (!CollectionUtils.isEmpty(perms))
                simpleAuthorizationInfo.addStringPermissions(perms);
            return simpleAuthorizationInfo;
        }
        else
            return null;
    }

    /**
     * 登录认证
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        //获取账户主体
        String principal = (String) authenticationToken.getPrincipal();
        System.out.printf("开始认证帐号:%s",principal);
        System.out.println();
        //查询该主体是否存在
        Account account=accountService.findByUsername(principal);
        if (!ObjectUtils.isEmpty(account)) {
            try {
                if (!account.isEnable()) {
                    throw new LockedAccountException("账号已被锁定,请联系管理员！");
                }
                SimpleAuthenticationInfo simpleAuthenticationInfo = new SimpleAuthenticationInfo(account.getUsername(),account.getPassword(),ByteSource.Util.bytes(account.getSalt()),getName());
                return simpleAuthenticationInfo;
            }
            catch (UnknownAccountException e) {
                System.out.println("用户名错误!");
                e.printStackTrace();
            }
            catch (IncorrectCredentialsException e)
            {
                System.out.println("密码错误!");
                e.printStackTrace();
            }
        }
        return null;
    }
}
