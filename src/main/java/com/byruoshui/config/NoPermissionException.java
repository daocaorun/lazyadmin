package com.byruoshui.config;

import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

/*
全局异常处理
 */
@ControllerAdvice
public class NoPermissionException {

    @ExceptionHandler(UnauthorizedException.class)
    public ModelAndView handleShiroException(Exception ex) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("errormsg","无访问权限");
        mv.setViewName("/admin/Unauthorized");
        return mv;
    }

    @ExceptionHandler(AuthorizationException.class)
    public ModelAndView AuthorizationException(Exception ex) {
        ModelAndView mv = new ModelAndView();
        mv.addObject("errormsg","权限验证失败");
        mv.setViewName("/admin/Unauthorized");
        return mv;
    }
}
