package com.byruoshui.utils;

import java.util.Random;

public class SaltUtils {

    /**
     * 生成随机盐Salt方法
     * @param n
     * @return
     */
    public static String getSalt(int n)
    {
        char[] chars="ABCDEFGHIJKLMNOPQISTUVWXYZabcdefghijklmnopqistuvwxyz~!@#$%^&*()_+=/-.<>?".toCharArray();
        StringBuffer stringBuffer=new StringBuffer();
        for (int i=0;i<n;i++)
        {
            char achar=chars[new Random().nextInt(chars.length)];
            stringBuffer.append(achar);
        }
        return stringBuffer.toString();
    }
}
