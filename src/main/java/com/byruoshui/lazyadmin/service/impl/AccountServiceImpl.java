package com.byruoshui.lazyadmin.service.impl;

import com.byruoshui.lazyadmin.entity.Account;
import com.byruoshui.lazyadmin.entity.PageParams;
import com.byruoshui.lazyadmin.mapper.AccountMapper;
import com.byruoshui.lazyadmin.mapper.RoleMapper;
import com.byruoshui.lazyadmin.service.AccountService;
import com.byruoshui.utils.PageRequest;
import com.byruoshui.utils.PageResult;
import com.byruoshui.utils.PageUtils;
import com.byruoshui.utils.SaltUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountMapper accountMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Override
    public Account findByUsername(String username) {
       return accountMapper.findByUsername(username);
    }

    @Override
    public boolean login(String username, String password) {
        Account account=accountMapper.login(username,password);
        return !ObjectUtils.isEmpty(account);
    }

    public Account findById(int id)
    {
        return accountMapper.findById(id);
    }

    @Override
    public void regiseter(Account account) {
        String salt= SaltUtils.getSalt(8);
        Md5Hash md5Hash=new Md5Hash(account.getPassword(),salt,1024);
        account.setPassword(md5Hash.toHex());
        account.setSalt(salt);
        accountMapper.register(account);
    }

    /**
     * 分页查询
     * @param pageRequest 自定义，统一分页查询请求
     * @return
     */
    @Override
    public PageResult findPage(PageRequest pageRequest) {
        return PageUtils.getPageResult(pageRequest, getPageInfo(pageRequest));
    }

    /**
     * 调用分页插件完成分页
     * @param pageRequest
     * @return
     */
    private PageInfo<Account> getPageInfo(PageRequest pageRequest) {
        int pageNum = pageRequest.getPageNum();
        int pageSize = pageRequest.getPageSize();
        PageHelper.startPage(pageNum, pageSize);
        List<Account> sysMenus = accountMapper.GetAccountByPage();
        return new PageInfo<Account>(sysMenus);
    }

    public void delAccount(int id)
    {
        accountMapper.delAccount(id);
    }

    public void resetPassword(int userId)
    {
        accountMapper.resetPassword(userId);
    }
}
