package com.byruoshui.lazyadmin.service.impl;

import com.byruoshui.lazyadmin.entity.Menu;
import com.byruoshui.lazyadmin.mapper.MenuMapper;
import com.byruoshui.lazyadmin.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class MenuServiceImpl implements MenuService {

    @Autowired
    private MenuMapper mapper;

    @Override
    public List<Menu> getAllMenu() {
        List<Menu> resultMenu=new ArrayList<Menu>();
        List<Menu> menuList=mapper.getAllMenu();
        AssemblyMenu(menuList,resultMenu);
        return resultMenu;
    }
    //菜单管理
    public List<Menu> menuManager()
    {
        List<com.byruoshui.lazyadmin.entity.Menu> resultMenu=new ArrayList<com.byruoshui.lazyadmin.entity.Menu>();
        List<com.byruoshui.lazyadmin.entity.Menu> menuList=mapper.menuManager();
        AssemblyMenu(menuList,resultMenu);
        return resultMenu;
    }

    @Override
    public List<Menu> findByPerm(int permID) {
        return mapper.findByPerm(permID);
    }

    @Override
    public List<Menu> findByRole(int roleID) {
        return mapper.findByRole(roleID);
    }


    private void AssemblyMenu(List<Menu> list,List<Menu> newList) {
        if (list.size()>0)
        {
            list.forEach(m->{
                if (m.getParentId()==0)
                    newList.add(m);
                else
                {
                    newList.forEach(p->{
                        if (m.getParentId()==p.getId())
                            p.getChildMenu().add(m);
                        else
                        {
                            p.getChildMenu().forEach(pc->{
                                if (m.getParentId()==pc.getId())
                                    pc.getChildMenu().add(m);
                            });
                        }
                    });
                }
            });
        }
    }

    /*
       增加菜单
    */
    public void addMenu(Menu menu)
    {
        mapper.addMenu(menu);
    }

    public void delMenu(int id)
    {
        mapper.delMenu(id);
    }
    public void delMenuAll(String ids)
    {
        mapper.delMenuAll(ids);
    }

    public Menu findById(int id)
    {
        return mapper.findById(id);
    }

    public Menu findByPid(int pid)
    {
        return mapper.findById(pid);
    }

    public void UpdateMenu(Menu menu)
    {
        mapper.UpdateMenu(menu);
    }
}
