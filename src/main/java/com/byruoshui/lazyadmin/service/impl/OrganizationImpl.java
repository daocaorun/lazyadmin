package com.byruoshui.lazyadmin.service.impl;

import com.byruoshui.lazyadmin.entity.Organization;
import com.byruoshui.lazyadmin.mapper.OrganizationMapper;
import com.byruoshui.lazyadmin.service.OrganizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import java.util.List;

@Service
public class OrganizationImpl implements OrganizationService {

    @Autowired
    private OrganizationMapper mapper;

    @Override
    public Organization findById(int id) {
        return mapper.findById(id);
    }

    @Override
    public List<Organization> findByPid(int pid) {
        return mapper.findByPid(pid);
    }

    @Transactional
    @Override
    public String AddOrganization(Organization organization)
    {
        String msg="保存成功";
        if (organization.getParentId() > 0) {
            Organization porg = mapper.findById(organization.getParentId());
            if (organization.getCode().equals(porg.getCode()))
                msg = "请输入部门编号.";
            else if (!organization.getCode().contains(porg.getCode()))
                msg = "部门编号输入有误,请重新输入.";
            else if (findByCode(organization.getCode(), organization.getId()))
                msg = "部门编号已存在,请重新输入.";
            else {
                if (!ObjectUtils.isEmpty(porg)) {
                    porg.setHaveChild(true);
                    mapper.EditOrganization(porg);
                }
            }
        }
        mapper.addOrganization(organization);
        return msg;
    }

    @Override
    public String EditOrganization(Organization organization)
    {
        String msg="保存成功";
        if (organization.getParentId()>0) {
            Organization porg = mapper.findById(organization.getParentId());
            if (organization.getCode().equals(porg.getCode()))
                msg = "保存失败,请输入部门编号.";
            else if (!organization.getCode().contains(porg.getCode()))
                msg = "部门编号输入有误,请重新输入.";
            else if (findByCode(organization.getCode(),organization.getId()))
                msg = "部门编号已存在,请重新输入.";
            else {
                mapper.EditOrganization(organization);
            }
        }
        return msg;
    }

    @Transactional
    @Override
    public void DelOrganization(int id) {
        Organization organization=mapper.findById(id);
        if (!ObjectUtils.isEmpty(organization))
        {
           Organization porganization=mapper.findById(organization.getParentId());
           if (!ObjectUtils.isEmpty(porganization))
           {
                if (mapper.findChildSize(porganization.getId())>1)
                    porganization.setHaveChild(true);
                else
                    porganization.setHaveChild(false);
               mapper.EditOrganization(porganization);
           }
            mapper.DelOrganization(organization.getId());
        }
    }

    @Override
    public boolean findByCode(String code,int id) {
        return mapper.findByCode(code,id);
    }
}
