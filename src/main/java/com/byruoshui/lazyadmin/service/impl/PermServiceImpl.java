package com.byruoshui.lazyadmin.service.impl;

import com.byruoshui.lazyadmin.entity.Perm;
import com.byruoshui.lazyadmin.mapper.PermMapper;
import com.byruoshui.lazyadmin.service.PermService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class PermServiceImpl implements PermService {

    @Autowired
    private PermMapper permMapper;
    @Override
    public List<Perm> findByRoleName(String roleName) {
        return permMapper.findByRoleName(roleName);
    }
}
