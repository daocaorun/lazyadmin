package com.byruoshui.lazyadmin.service.impl;

import com.byruoshui.lazyadmin.entity.Role;
import com.byruoshui.lazyadmin.mapper.RoleMapper;
import com.byruoshui.lazyadmin.service.PermService;
import com.byruoshui.lazyadmin.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleMapper roleMapper;
    private PermService permService;

    @Override
    public List<Role> findByAccountName(String accName) {
       return roleMapper.findByAccountName(accName);
    }

    @Override
    public List<Role> findByAccountID(int accid) {
        return roleMapper.findByAccountID(accid);
    }

    @Override
    public Role findById(int id) {
        return roleMapper.findById(id);
    }

    @Override
    public Role findByName(String roleName) {
        return roleMapper.findByName(roleName);
    }
}
