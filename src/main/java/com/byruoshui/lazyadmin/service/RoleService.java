package com.byruoshui.lazyadmin.service;

import com.byruoshui.lazyadmin.entity.Role;

import java.util.List;

public interface RoleService {
    List<Role> findByAccountName(String accName);
    List<Role> findByAccountID(int accid);
    Role findById(int id);
    Role findByName(String roleName);
}
