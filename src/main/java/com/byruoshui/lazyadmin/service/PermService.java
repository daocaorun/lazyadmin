package com.byruoshui.lazyadmin.service;

import com.byruoshui.lazyadmin.entity.Perm;

import java.util.List;

public interface PermService {
    List<Perm> findByRoleName(String roleName);
}
