package com.byruoshui.lazyadmin.service;

import com.byruoshui.lazyadmin.entity.Organization;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface OrganizationService {

    Organization findById(int id);

    List<Organization> findByPid(int pid);

    String AddOrganization(Organization organization);
    String EditOrganization(Organization organization);
    void DelOrganization(int id);
    boolean findByCode(String code,int id);
}
