package com.byruoshui.lazyadmin.service;


import com.byruoshui.lazyadmin.entity.Menu;

import java.util.List;

public interface MenuService {
    /*
        查询所有的菜单数据
     */
    List<Menu> getAllMenu();

    List<Menu> menuManager();
    /*
        根据权限ID获取菜单数据
     */
    List<Menu> findByPerm(int permID);
    /*
        根据角色ID获取菜单数据
     */
    List<Menu> findByRole(int roleID);

    /*
       增加菜单
    */
    void addMenu(Menu menu);

    void delMenu(int id);
    void delMenuAll(String ids);

    Menu findById(int id);

    Menu findByPid(int pid);

    void UpdateMenu(Menu menu);
}
