package com.byruoshui.lazyadmin.mapper;

import com.byruoshui.lazyadmin.entity.Role;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RoleMapper {

    List<Role> findByAccountName(String accName);
    List<Role> findByAccountID(int accid);
    Role findById(int id);
    Role findByName(String roleName);
}
