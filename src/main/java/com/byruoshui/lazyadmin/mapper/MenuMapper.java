package com.byruoshui.lazyadmin.mapper;

import com.byruoshui.lazyadmin.entity.Menu;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MenuMapper {
    /*
        查询所有的菜单数据
     */
    List<Menu> getAllMenu();
    List<Menu> menuManager();

    Menu findById(int id);
    Menu findByPid(int pid);
    /*
        根据权限ID获取菜单数据
     */
    List<Menu> findByPerm(int permID);
    /*
        根据角色ID获取菜单数据
     */
    List<Menu> findByRole(int roleID);

    /*
        增加菜单
     */
    void addMenu(Menu menu);

    /*
    删除菜单
     */
    void delMenu(int id);
    /*
    删除菜单
     */
    void delMenuAll(String ids);

    void UpdateMenu(Menu menu);
}
