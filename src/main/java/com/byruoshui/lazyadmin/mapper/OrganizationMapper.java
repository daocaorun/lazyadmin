package com.byruoshui.lazyadmin.mapper;

import com.byruoshui.lazyadmin.entity.Organization;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface OrganizationMapper {
    Organization findById(int id);
    List<Organization> findByPid(int pid);
    boolean findByCode(@Param("code") String code,@Param("id") int id);
    void addOrganization(Organization organization);
    void EditOrganization(Organization organization);
    void DelOrganization(int id);
    int findChildSize(int pid);
}
