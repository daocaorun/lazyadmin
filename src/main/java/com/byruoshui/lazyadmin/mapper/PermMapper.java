package com.byruoshui.lazyadmin.mapper;

import com.byruoshui.lazyadmin.entity.Perm;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface PermMapper {
    List<Perm> findByRoleName(String roleName);
}
