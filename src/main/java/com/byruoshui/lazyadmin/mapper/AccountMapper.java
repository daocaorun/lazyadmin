package com.byruoshui.lazyadmin.mapper;


import com.byruoshui.lazyadmin.entity.Account;
import com.byruoshui.lazyadmin.entity.PageParams;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface AccountMapper {

    Account findByUsername(String username);

    Account findById(int id);

    Account login(String username,String password);

    void register(Account account);

    List<Account> GetAccountByPage();

    void delAccount(int id);

    void resetPassword(int userId);
}
