package com.byruoshui.lazyadmin.entity;

import lombok.Data;

import java.util.List;

@Data
public class Account{
    private int id;
    private String username;
    private String password;
    private String salt;
    private String interations;
    private boolean enable;
    private List<Role> roles;
    private String createDate;
    private int deptId;
    private String dptName;
    private  String realName;
    private  String phoneNumber;
    private  String landlineNumber;
    private  String email;
    private int sex;
}
