package com.byruoshui.lazyadmin.entity;

import lombok.Data;

import java.util.List;

@Data
public class Role {
    private int id;
    private String name;
    private String code;
    private int sort;
    private boolean enable;
    private List<Perm> perms;
}
