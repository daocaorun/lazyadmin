package com.byruoshui.lazyadmin.entity;

import lombok.Data;

@Data
public class PageParams {

    private int page;
    private int limit;
}
