package com.byruoshui.lazyadmin.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class Menu {
    private int id;
    private int parentId;
    private String name;
    private String url;
    private String icon;
    private int sort;
    private int permId;
    private boolean enable;
    private  boolean expand;
    private List<Menu> childMenu;

    private Menu()
    {
        childMenu=new ArrayList<>();
    }

    private int type;
    private String createDateTime;
}
