package com.byruoshui.lazyadmin.entity;

import lombok.Data;

@Data
public class Perm {
    private int id;
    private int parentId;
    private String name;
    private String code;
    private int sort;
    private boolean enable;
}
