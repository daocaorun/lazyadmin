package com.byruoshui.lazyadmin.entity;

import lombok.Data;

import java.util.Date;

@Data
public class Organization {

    private int id;
    private int parentId;
    private int level;
    private String code;
    private String name;
    private int sort;
    private boolean enable;
    private String createDateTime;
    private boolean haveChild;
}
