package com.byruoshui.lazyadmin.Controller;

import com.byruoshui.lazyadmin.entity.Account;
import com.byruoshui.lazyadmin.service.AccountService;
import com.byruoshui.utils.R;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/login")
public class LoginController {
    @Autowired
    private AccountService accountService;

    @GetMapping()
    public String login() {
        return "/admin/login";
    }

    @GetMapping("register")
    public String register() {
        return "/admin/register";
    }

    @PostMapping("login")
    @ResponseBody
    public R dologin(String username, String password,boolean rememberMe) {
        try {
            UsernamePasswordToken token = new UsernamePasswordToken(username, password,rememberMe);
            Subject subject = SecurityUtils.getSubject();
            subject.login(token);
            return R.ok("登录成功");
        } catch (Exception e) {
            e.printStackTrace();
            R.error(1,e.getMessage());
        }
        return R.error("登录失败,用户名或密码错误!");
    }

    @PostMapping("register")
    @ResponseBody
    public R doregister(Account account) {
        System.out.println("注册action");
        try {
            accountService.regiseter(account);
           return R.ok("注册成功");
        } catch (Exception e) {
            e.printStackTrace();
            return R.error(1,e.getMessage());
        }
    }
}
