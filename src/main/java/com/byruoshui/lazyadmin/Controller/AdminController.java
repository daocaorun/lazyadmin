package com.byruoshui.lazyadmin.Controller;

import com.byruoshui.utils.R;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @GetMapping
    public String index() {
        return "/admin/index";
    }


    @GetMapping("logout")
    @ResponseBody
    public R logout()
    {
        SecurityUtils.getSubject().logout();
        return R.ok();
    }

    @GetMapping("console")
    public String console()
    {
        return "/admin/home/console";
    }

}