package com.byruoshui.lazyadmin.Controller;

import com.byruoshui.lazyadmin.entity.Organization;
import com.byruoshui.lazyadmin.service.OrganizationService;
import com.byruoshui.utils.R;
import org.apache.shiro.util.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
@RequestMapping("/admin/Organization")
public class OrganizationController {

    @Autowired
    private OrganizationService organizationService;

    /**
     * 组织机构列表
     * @return
     */
    @GetMapping()
   public String index()
   {
       return "/admin/Organization/organizationList";
   }

    /**
     * 新增组织机构
     * @return
     */
   @GetMapping("AddOrganization")
   public ModelAndView AddOrganization(int id)
   {
       ModelAndView mv=new ModelAndView();
       if (id>0)
       {
           Organization organization=organizationService.findById(id);
           mv.addObject("pOrganization",organization);
       }
       mv.setViewName("/admin/Organization/addOrganization");
       return mv;
   }

    /**
     * 编辑组织机构
     * @return
     */
    @GetMapping("EditOrganization")
    public ModelAndView EditOrganization(int id)
    {
        ModelAndView mv=new ModelAndView();
        if(id>0)
        {
            Organization organization=organizationService.findById(id);
            Organization porganization=organizationService.findById(organization.getParentId());
            mv.addObject("organization",organization);
            mv.addObject("porganization",porganization);
        }
        mv.setViewName("/admin/Organization/editOrganization");
        return mv;
    }

    @GetMapping("Manager")
    @ResponseBody
    public R Manager(int pid)
    {
        try {
            List<Organization> menus=organizationService.findByPid(pid);
            if (!CollectionUtils.isEmpty(menus))
            {
                return R.success(0,menus.size(),"获取成功",menus);
            }
            else
                return R.failure("没有数据");
        }
        catch (Exception e)
        {
            return R.error(e.getMessage());
        }
    }

    @PostMapping(value="AddOrganization",produces = "application/json; charset=utf-8")
    @ResponseBody
    public R AddOrganization(Organization organization)
    {
        String msg;
        try {
            if (!ObjectUtils.isEmpty(organization)) {
                msg=organizationService.AddOrganization(organization);
                if (!msg.equals("保存成功"))
                    return R.failure(msg);
                else
                    return R.ok("保存成功");
            } else {
                return R.failure("要保存的数据为空,请重新输入.");
            }
        }
        catch (Exception ex)
        {
            return R.error(ex.getMessage());
        }
    }

    @PostMapping(value="EditOrganization",produces = "application/json; charset=utf-8")
    @ResponseBody
    public R EditOrganization(Organization organization)
    {
        String msg;
        try {
            if (!ObjectUtils.isEmpty(organization)) {
                msg=organizationService.EditOrganization(organization);
                if (!msg.equals("保存成功"))
                    return R.failure(msg);
                else
                    return R.ok("保存成功");
            } else {
                return R.error("要保存的数据为空,请重新输入.");
            }
        }
        catch (Exception ex)
        {
            return R.error(ex.getMessage());
        }
    }

    @PostMapping(value="DelOrganization",produces = "application/json; charset=utf-8")
    @ResponseBody
    public R DelOrganization(int id)
    {
        try {
            if (id>0) {
                organizationService.DelOrganization(id);
                return R.ok("删除成功");
            } else {
                return R.error("要删除的数据为空.");
            }
        }
        catch (Exception ex)
        {
            return R.error(ex.getMessage());
        }
    }
}
