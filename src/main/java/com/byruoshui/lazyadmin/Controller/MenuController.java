package com.byruoshui.lazyadmin.Controller;

import com.byruoshui.lazyadmin.entity.Menu;
import com.byruoshui.lazyadmin.service.MenuService;
import com.byruoshui.utils.R;
import org.apache.shiro.util.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.beans.ExceptionListener;
import java.util.List;

@Controller
@RequestMapping("/admin/menu")
public class MenuController {

    @Autowired
    private MenuService menuService;

    @GetMapping
    public String index(Model model)
    {
//        List<Menu> menus=menuService.getAllMenu();
//        if (!CollectionUtils.isEmpty(menus))
//            model.addAttribute("menuList",menus);
//        else
//            model.addAttribute("menuList",null);
        return "/admin/menu/menuList";
    }


    @PostMapping("getAllMenu")
    @ResponseBody
    public R getAllMenu()
    {
        try {
            List<Menu> menus=menuService.getAllMenu();
            if (!CollectionUtils.isEmpty(menus))
            {
                return R.success(0,menus.size(),"获取成功",menus);
            }
            else
                return R.failure("没有数据");
        }
        catch (Exception e)
        {
            return R.error(e.getMessage());
        }
    }

    @PostMapping("MenuManager")
    @ResponseBody
    public R MenuManager()
    {
        try {
            List<Menu> menus=menuService.menuManager();
            if (!CollectionUtils.isEmpty(menus))
            {
                return R.success(0,menus.size(),"获取成功",menus);
            }
            else
                return R.failure("没有数据");
        }
        catch (Exception e)
        {
            return R.error(e.getMessage());
        }
    }


    @PostMapping("findByPerm")
    @ResponseBody
    public R findByPerm(int permId)
    {
        try {
            List<Menu> menus=menuService.findByPerm(permId);
            if (!CollectionUtils.isEmpty(menus))
            {
                return R.success("获取成功",menus);
            }
            else
                return R.failure("没有数据");
        }
        catch (Exception e)
        {
            return R.error(e.getMessage());
        }
    }

    @PostMapping("findByRole")
    @ResponseBody
    public R findByRole(int roleId)
    {
        try {
            List<Menu> menus=menuService.findByRole(roleId);
            if (!CollectionUtils.isEmpty(menus))
            {
                return R.success("获取成功",menus);
            }
            else
                return R.failure("没有数据");
        }
        catch (Exception e)
        {
            return R.error(e.getMessage());
        }
    }

    @GetMapping("AddMenu")
    public ModelAndView AddMenu(int id)
    {
        ModelAndView mv=new ModelAndView();
        if (id>0)
        {
            Menu menu=menuService.findById(id);
            mv.addObject("pMenu",menu);
        }
        mv.setViewName("/admin/menu/addMenu");
        return mv;
    }

    @GetMapping("EditMenu")
    public ModelAndView EditMenu(int id)
    {
        ModelAndView mv=new ModelAndView();
        if (id!=0)
        {
            Menu menu=menuService.findById(id);
            Menu pmenu=menuService.findById(menu.getParentId());
            mv.addObject("MenuInfo",menu);
            mv.addObject("pMenuInfo",pmenu);
        }
        mv.setViewName("/admin/menu/editMenu");
        return mv;
    }

    /**
     *  新增菜单
     * @return
     */
    @PostMapping("addMenu")
    @ResponseBody
    public R AddMenu(Menu menu)
    {
        try {
            if (!ObjectUtils.isEmpty(menu)) {
                menuService.addMenu(menu);
                return R.ok("保存成功");
            } else {
                return R.error("要保存的菜单数据为空,请重新输入.");
            }
        }
        catch (Exception ex)
        {
            return R.error(ex.getMessage());
        }
    }

    @PostMapping("EditMenu")
    @ResponseBody
    public R EditMenu(Menu menu)
    {
        try {
            if (!ObjectUtils.isEmpty(menu)) {
                menuService.UpdateMenu(menu);
                return R.ok("保存成功");
            } else {
                return R.error("要保存的菜单数据为空,请重新输入.");
            }
        }
        catch (Exception ex)
        {
            return R.error(ex.getMessage());
        }
    }

    @PostMapping(value="delMenu",produces = "application/json; charset=utf-8")
    @ResponseBody
    public R delMenu(int id)
    {
        try {
                menuService.delMenu(id);
                return R.ok("删除成功");
        }
        catch (Exception ex)
        {
            return R.error(ex.getMessage());
        }
    }

    @PostMapping("delMenuAll")
    @ResponseBody
    public R delMenuAll(String ids)
    {
        try {
            menuService.delMenuAll(ids);
            return R.ok("删除成功");
        }
        catch (Exception ex)
        {
            return R.error(ex.getMessage());
        }
    }
}
