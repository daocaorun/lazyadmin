package com.byruoshui.lazyadmin.Controller;

import com.byruoshui.lazyadmin.entity.Account;
import com.byruoshui.lazyadmin.service.AccountService;
import com.byruoshui.utils.PageRequest;
import com.byruoshui.utils.PageResult;
import com.byruoshui.utils.R;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/admin/account")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping()
    public String index()
    {
        return "/admin/user/userList";
    }

    @GetMapping("AddUserInfo")
    public String AddUserInfo()
    {
        return "/admin/user/addUserInfo";
    }

    @GetMapping("EditUserInfo")
    public ModelAndView EditUserInfo(int id)
    {
        ModelAndView modelAndView=new ModelAndView();
        Account account=accountService.findById(id);
        if (!ObjectUtils.isEmpty(account))
            modelAndView.addObject("userInfo",account);
        modelAndView.setViewName("/admin/user/editUserInfo");
        return modelAndView;
    }

    @GetMapping("AccountDetail")
    public ModelAndView Detail(int id)
    {
        ModelAndView modelAndView=new ModelAndView();
        Account account=accountService.findById(id);
        if (!ObjectUtils.isEmpty(account))
            modelAndView.addObject("userInfo",account);
        modelAndView.setViewName("/admin/user/accountDetail");
        return modelAndView;
    }

    @RequestMapping("GetAccountList")
    @ResponseBody
    public R GetAccountList(PageRequest pageQuery)
    {
        PageResult pageResult=accountService.findPage(pageQuery);
        if (!ObjectUtils.isEmpty(pageResult))
            return R.success(0,Integer.parseInt(String.valueOf(pageResult.getTotalSize())),"获取成功",pageResult.getContent());
        else
            return R.success(1,0,"没有数据",null);
    }

    @PostMapping(value="register",produces = "application/json; charset=utf-8")
    @ResponseBody
    public R register(Account account)
    {
        try {
            if (!ObjectUtils.isEmpty(account)) {
                accountService.regiseter(account);
                return R.ok(0, "注册成功");
            }
            else
            {
                return R.ok(1,"注册失败,注册数据为空.");
            }
        }
        catch (Exception ex)
        {
            return R.error(ex.getMessage());
        }
    }

    @PostMapping("delAccount")
    @ResponseBody
    public R delAccount(int id)
    {
       try
       {
           if (!ObjectUtils.isEmpty(id)&&id>0)
           {
            accountService.delAccount(id);
            return  R.ok("删除成功");
           }
           else
               return R.failure("删除失败,数据为空.");
       }
       catch (Exception ex)
       {
            return R.error(ex.getMessage());
       }
    }
}